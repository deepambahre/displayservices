
import './App.css';
import "./style.css";
import Services from './Components/Services';

function App() {
  return (
    <div className="App">
      <Services/>
    </div>
  );
}

export default App;
