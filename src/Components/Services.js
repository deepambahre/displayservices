import React from 'react'
import axios from 'axios';
import $ from "jquery";
export default function Services() {

const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
const URL = 'https://fir-dynamiclinks-e43dd.web.app/practical-api.json';

axios.get(PROXY_URL+URL)
.then( response => getResult(response) )
.catch(e => console.error( "Error occured! ", e));
     
function getResult(result){
    var data2= result.data.data.purchased_services;
    //console.log(data2);
    for(var j =0; j< data2.length; j++){
        //console.log(data2[j]);
        var data3=data2[j].purchased_office_template.purchased_office_services;
        //console.log(data3);
        for(var k =0; k< data3.length; k++){
            //console.log(data3[k]);
            $("#result").append(`
            <div class="container-fluid">
            <h3 class="main-result">PURCHASED SERVICES</h3>
              <p class="main-result">${data2[j].name}</p>
              <div class="service-blocks">
                    <div class="row">
                        <div class="col-md-2 col-xs-12">
                            <div class="service-image">
                                <img src="${data3[k].image}" alt="${data3[k].image}" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <div class="service-text">
                               <p><span><strong>${data3[k].name}</strong></span><span class="move-right"><strong>Kr ${data3[k].price},-</strong></span></p>
                               <p>${data3[k].description}</p> 
                            </div>
                        </div>
                    </div>
                 </div>
              </div>
            
            `)
        }
    }
}
    return (
        <div>
            <div id="result" className="">

            </div>
        </div>
    )
}
